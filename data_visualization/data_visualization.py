"""Main module."""
import os
import altair as alt
import holoviews as hv
import numpy as np
from bokeh.plotting import show  # noqa
from matplotlib import pylab as plt

from . import datajoint_helpers as dj_helpers
from altair.vegalite.v4.schema import Undefined


def regression_with_r2(df, x='stim_amplitude', y='%clipping', yscale=None, xscale=None, groupby=None, **kwargs):
    """
    Example of kwargs:

    method="poly", order=2,extent=[1.4e5,5.5e5]
    """

    # Points
    if not yscale:
        yscale = alt.Scale(domain=[0, 40])
    chart0 = alt.Chart(df).mark_point(fill=None, opacity=.6)
    if groupby:
        chart0 = chart0.encode(
            x=alt.X(x, scale=xscale or alt.Scale(zero=False)),
            y=alt.Y(y, scale=yscale, title=y),
            color=groupby,
        ).properties(width=700)
    else:
        chart0 = chart0.encode(
            x=alt.X(x, scale=xscale or alt.Scale(zero=False)),
            y=alt.Y(y, scale=yscale, title=y),
        ).properties(width=700)

    # Reg line
    chart = chart0 + chart0.transform_regression(x, y,
                                                 groupby=[groupby] if groupby else Undefined,
                                                 #                                  extent=[1.5e5,5.5e5]
                                                 **kwargs).mark_line(fill=None)
    if 'threshold' in df.columns:
        chart += alt.Chart(df).mark_line(color='red', fill=None).encode(
            x=x,
            y='threshold',
        )
    # Get r2
    regression_result = chart0.transform_regression(x, y, params=True,
                                                    groupby=[groupby] if groupby else Undefined,
                                                    **kwargs
                                                    )
    # Plot r2
    params = regression_result.mark_text(align='left').encode(
        x=alt.value(20),  # pixels from left
        y=alt.value(20),  # pixels from top
        text=f'mean(rSquared):N'
    )
    chart += params

    return chart.interactive()


def altair_setup(fig_format=['png'], scale_factor=2.0):
    alt.data_transformers.disable_max_rows()
    # Too much detail for fancy rendering:
    if not os.getenv('CI_PROJECT_DIR'):
        alt.renderers.enable('altair_saver', fmts=fig_format, scale_factor=scale_factor)
    # New categorical theme:
    alt.themes.register('my_theme', lambda: {'config': {'range': {'category': {'scheme': 'category10'}}}})
    alt.themes.enable('my_theme')


def altair_robust_scale(column, quantiles=(.2, .8)):
    """
    eg:
    y='coherence_magnitude_at'

    alt.Chart(df).mark_line().encode(
        y=alt.Y(y, scale=dviz.altair_robust_scale(df[y])),
    )
    :param column:
    :param quantiles:
    :return:
    """
    return alt.Scale(zero=False,
                     domain=column.quantile(quantiles).values,
                     clamp=True
                     )


def plot_trace_interactive(xar, ylim=(-1000, 1000), title=None, subsample=30):
    #                 xar.plot(color='brain_region');
    #                 plt.title(f'{subject} - {fid[20:40]} ')
    #                 plt.show()
    if title is None:
        title = (f'Recording {xar["fid"].values} '
                 f't0={xar["tstart"].values}')
        try:
            title += f'(std={np.std(xar.values[:, -1]).round(2)})'
        except IndexError:
            title += f'(std={np.std(xar.values).round(2)})'

    figure = xar.sel(Time=slice(None, None, subsample)
                     ).hvplot.line(
        by='brain_region',
        ylim=ylim,
        alpha=.6,
        width=1300,
        title=title
    ).opts(active_tools=['box_zoom'])
    show(hv.render(figure))


def plot_trace_from_blobs(df, **kwargs):
    """
    df = (RecordingSessionParameters * MazeTrials * EventRelatedPotential
      & 'event="trial_without_iti"').head(6, format='frame').reset_index()
    df['phase_delay_frac'] = df['phase_delay_frac'].astype(float)
    df['frequency'] = df['frequency'].astype(float)

    for fid, ddf in df.groupby('fid'):
        for i,dddf in ddf.groupby('tstart'): # loop over trials
        plot_trace_from_blobs(dddf)

    :param df: dataframe with 'signal' blobs in rows
    :return:
    """
    xar = dj_helpers.stack_blobs(df,
                                 fs=df.iloc[0]['lfp_fs']
                                 ).to_xarray()
    xar = xar.squeeze()['signal'].astype(float)
    if 'laser' in xar['brain_region']:
        xar[{'brain_region': 0}] = xar.sel(brain_region='laser').values - 1000  # Bring laser to scale with pfc and hpc
    plot_trace_interactive(xar, **kwargs)
    return xar


def plot_power_two_scales(sgram, br=None, fid=None, tstart=None, title=None):
    plt.figure(figsize=[14, 5])
    plt.subplot(1, 2, 1)
    sgram.sel(Frequency=slice(5, 35)).plot(robust=True)
    plt.subplot(1, 2, 2)
    sgram.sel(Frequency=slice(5, 125)).plot(robust=True)
    if not title:
        title = f'{br} - rec: {fid} - tstart:{tstart}'
    plt.title(title)
    plt.tight_layout()
    plt.show()


def nested_facet(make_chart,
                 data,
                 column: str = None,
                 column1: str = None,
                 row1: str = None,
                 row2: str = None,
                 width: int = 80,
                 height: int = 150,
                 finalize: bool = True
                 ) -> alt.Chart:
    """

    Returns
    -------
    object
    """
    if column is None and row is None:
        return base_chart  # Nothing to do
    assert data is not None
    if column:
        assert column in data.columns, f'{column} is not in {data.columns}'
        # sanitize a little:
        data[column] = data[column].astype(str)
    if row1:
        assert row1 in data.columns, f'{row1} is not in {data.columns}'
        # sanitize a little:
        data[row1] = data[row1].astype(str)

    charts = []
    for i_col, ((colval1, colval2), subdata1) in enumerate(data.groupby([column, column1])):
        charts.append(alt.vconcat(*[make_chart(subdata,
                                               x_axis=(i_row == 0),
                                               y_axis=(i_col == 0)
                                               ).properties(
            title=[f'{row1[0]}:{rowval1}({row2[0]}:{rowval2})', f'{colval1}({colval2})']
        )
            for i_row, ((rowval1, rowval2), subdata) in enumerate(subdata1.groupby([row1, row2]))])
                      )
    chart = alt.hconcat(*charts)

    if finalize:  # TODO is there a way to do this in theme instead?
        chart = chart.configure_view(
            stroke=None
        )
    return chart
