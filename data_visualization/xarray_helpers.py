import xarray as xr
import pandas as pd


def expand_attrs(sg, take_attributes=None, exclude_attributes=None):
    take_attributes = take_attributes or sg.attrs.keys()
    exclude_attributes = exclude_attributes or list()
    attrs = {attr: sg.attrs[attr] for attr in take_attributes
             if type(sg.attrs[attr]) != dict
             and 'fs' not in attr
             and 'filter' not in attr
             and 'std' not in attr
             and 'std' not in attr
             and 'power' not in attr
             and 'date' not in attr
             and attr not in ('tstart', 'tend', 'promoter', 'strain', 'ievent',
                              'i_maze_trial', 'maze_trial_duration', 'stim_amplitude')
             and attr not in exclude_attributes
             }
    return sg.assign_coords(attrs).expand_dims(list(attrs.keys()))


def to_xarray(row, **kwargs):
    row['spectrogram']['attrs'] = row.drop('spectrogram')
    xar = xr.DataArray.from_dict(row['spectrogram'])
    return expand_attrs(xar, **kwargs)


def squeeze_nans(xar):
    xar=xar.squeeze()
    for dim in xar.dims:
        xar=xar.dropna(dim, 'all').squeeze()
    return xar



import xarray as xr
import pandas as pd
def stack_xarrays_from_blobs(df, y='Spectrogram', index_cols=['Brain region','Stim type','Stim frequency']):
    def process_row(ddf):
        try:
            xar=xr.DataArray.from_dict(ddf[y])
        except ValueError:
            xar=xr.Dataset.from_dict(ddf[y])
            
        for col in index_cols:
            xar[col]=ddf[col]
        return xar.to_dataframe()

    xars=[process_row(row) for _, row in df.iterrows()]
    
            
    df= pd.concat(xars).reset_index().set_index(list(xars[-1].index.names)+index_cols)
    try:
        return df.to_xarray()
    except ValueError as e:
        print(f'Cant make xarray from a Non-unique index. Try fewer rows? returning dataframe')
        return df

