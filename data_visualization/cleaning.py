import xarray as xr
import numpy as np

def std_hz(row, hz=60):
    sgram = xr.DataArray.from_dict(row)
    noise = sgram.sel(Frequency=hz, tolerance=15, method='nearest').values
    return np.std(noise)


def quality_metric(df, metric='std'):
    """
    eg
    df[std_1hz] = quality_metric(df, 'std_60hz')
    df[std_1hz] = quality_metric(df, 'std_1hz')

    :param df:
    :param metric: std, std
    :return:
    """
    if metric == 'std':
        return df['signal'].apply(lambda row: np.std(row[0]))

    elif metric == 'std_60hz':
        if 'spectrogram' not in df.columns:
            raise KeyError(f'Need spectrogram column for metric {metric}')

        return df['spectrogram'].apply(lambda row: std_hz(row, hz=60))
    elif metric == 'std_1hz':
        if 'spectrogram' not in df.columns:
            raise KeyError(f'Need spectrogram column for metric {metric}')

        return df['spectrogram'].apply(lambda row: std_hz(row, hz=1))

    else:
        raise SyntaxError(f'No such metric {metric}')
