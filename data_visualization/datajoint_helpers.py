import inflection
import numpy as np
import pandas as pd


def blob_row_to_long(df_blob, fs, y='signal', index_cols=None):
    if index_cols is None:
        index_cols = set(df_blob.keys()) - {y} - set(df_blob.keys()[df_blob.keys().str.contains('signal')])
    # assert type(df_blob is pd.Series()) or (df_blob.shape[0] == 1), f'Need exactly one row! Got {df_blob.shape}'

    # Make dataframe holding blob
    try:
        long_df_one_row = pd.DataFrame(df_blob[y], columns=[y])
    except Exception as e:
        long_df_one_row = pd.DataFrame(df_blob[y].squeeze(), columns=[y])

    long_df_one_row.insert(0, 'Time', np.arange(long_df_one_row.shape[0]) / fs)

    # Add index to dataframe
    [long_df_one_row.insert(0, col, val) for col, val in df_blob[index_cols].items()]
    return long_df_one_row


def stack_blobs(df, fs, y='signal', index_cols=None, sample=1):
    # Downsample:
    from scipy.signal import decimate
    df[y]=df[y].apply(lambda x: decimate(x.squeeze(), sample))
    # df[y]=df[y].apply(lambda x: x.squeeze()[::sample])

    if index_cols is None:
        index_cols = set(df.columns) - {y} - set(df.keys()[df.keys().str.contains('ignal')])
        
    df_long = pd.concat([blob_row_to_long(row_with_blob,
                                          fs=fs/sample,
                                          y=y,
                                          index_cols=index_cols) for i, row_with_blob in df.iterrows()
                         ])
    return df_long.set_index(list(set(index_cols).union({'Time'})))


def humanize_string(string='power_at_stim_frequency'):
    """

    :param string:
    :return:
    """

    columns = {'Pfc': 'PFC', 'Pfc tetrode': "PFC'", 'V hpc': 'vHPC',  # 'Signal': 'Laser power',
               'Pfc signal': 'PFC LFP', 'Stim amplitude': 'Cloop multiplier',
               'Pfc lfp phase': 'PFC theta phase', 'Hpc lfp phase': 'vHPC theta phase',
               'Is correct': 'Performance', 'Frequency': 'Stim frequency',
               'Phase delay frac': 'Stim phase', 'Cloop straight': 'Closed-loop',
               'Post cloop straight': 'No stim', '0': 'No stim',
               'Log power mean near': 'Log power near', 'Power mean near': 'Power near',
               'Log power mean at': 'Log power at', 'Power mean at': 'Power at',
               'Log coherence mean at': 'Log coherence at', 'Coherence mean at': 'Coherence at',
               'Log coherence mean near': 'Log coherence near', 'Coherence mean near': 'Coherence near',

               }
    human = inflection.humanize(string)
    if human in columns:
        human = columns[human]
    return human


def humanize_df(df, do_values_search=True, do_add_invert=True):
    """
    Renamce datajoint fields; humanize from_underscore To human readable
    :param do_values_search:
    :type do_values_search:
    :param df:
    :return:
    """

    assert (df.shape[0] > 0), 'Dataframe is empty'
    try:
        df.columns = [humanize_string(col) for col in df.columns]
        # df = df.rename(columns=columns, errors='ignore')
        df['Stim phase'] = df['Stim phase'].astype(float)
        df['Stim frequency'] = df['Stim frequency'].astype(float)
        df['Cloop multiplier'] = df['Cloop multiplier'].astype(float)
        df['Stim amplitude'] = df['Stim amplitude'].astype(float)
    except (TypeError, AttributeError, KeyError):  # KeyError when no Stim amplitude
        # print(e)  # means series, not dataframe
        pass
    if do_values_search:
        df['Stim type'] = df['Stim type'].replace({
            'oloop_coupled': 'Open-loop',
            'oloop_coupled_baseline-osc_phase-resets': 'Open-loop phase resets',
            'oloop_coupled_baseline-osc_phase-resets_twoway': 'Open-loop twoway',
            'cloop_coupled_baseline-osc_phase_resets_twoway_noisy_synapse_to_pfc': 'Twoway closed-loop',
            'cloop': 'Closed-loop',
            'cloop_straight': 'Closed-loop', 'post_cloop_straight': 'No stim',
            'cloop_invert': 'Closed-loop (i)', 'post_cloop_invert': 'No stim (i)',
            '0': 'No stim', 'oloop': 'Sine stim', "post_oloop": "No stim", 'baseline': 'Baseline'})

        if 'Session stim type' in df.columns:
            df['Session stim type'] = df['Session stim type'].replace({
                'oloop_coupled': 'Open-loop',
                'oloop_coupled_baseline-osc_phase-resets': 'Open-loop phase resets',
                'oloop_coupled_baseline-osc_phase-resets_twoway': 'Open-loop twoway',
                'cloop_coupled_baseline-osc_phase_resets_twoway_noisy_synapse_to_pfc': 'Twoway closed-loop',
                'cloop': 'Closed-loop',
                'cloop_straight': 'Closed-loop', 'post_cloop_straight': 'No stim',
                'cloop_invert': 'Closed-loop (i)', 'post_cloop_invert': 'No stim (i)',
                '0': 'No stim', 'oloop': 'Sine stim', "post_oloop": "No stim", 'baseline': 'Baseline'})

        if 'Brain region' in df.columns:
            df['Brain region'] = df['Brain region'].replace({'pfc': 'PFC', 'pfc_tetrode': "PFC'", 'v_hpc': 'vHPC'})

        # Handle inversion
        if do_add_invert:  # Otherwise will have 'Closed-loop (i)' stim type
            # # Drop blob-containing columns:
            # for col in df.columns:
            #     if type(df[col].iloc[0]) in [np.ndarray]:
            #         df.drop(col, axis=1, inplace=True)
            df['Inversion'] = df['Stim type'].str.contains('\(i\)')
            for col in df.columns:
                if 'tim type' in col:
                    df[col] = df[col].replace({'Closed-loop (i)': 'Closed-loop', 'No stim (i)': 'No stim', 0.22: 0.25})

    return df
