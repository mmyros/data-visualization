==================
Data visualization
==================


.. image:: https://img.shields.io/pypi/v/data_visualization.svg
        :target: https://pypi.python.org/pypi/data_visualization

.. image:: https://img.shields.io/travis/mmyros/data_visualization.svg
        :target: https://travis-ci.com/mmyros/data_visualization

.. image:: https://readthedocs.org/projects/data-visualization/badge/?version=latest
        :target: https://data-visualization.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Dataviz for neuroscience 


* Free software: MIT license
* Documentation: https://data-visualization.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
